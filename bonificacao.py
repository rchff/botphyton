import os


class Bonificacao:
    def __init__(self):
        self.codigo = None

    def IniciarCFOP(self):
        return f'''O seu cliente mora no seu estado, ou é de outro?{os.linesep}{os.linesep}/Mesmo{os.linesep}/Outro'''

    def GetCFOP(self, mensagem):

        AResposta = 'A cfop que você pode utilizar é: '

        if (mensagem == 'mesmo'):
            AResposta += f'''5910{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''

        elif (mensagem == 'outro'):
            AResposta += f'''6910{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''

        elif (mensagem == 'sim'):
            AResposta = f'''O seu cliente mora no seu estado, ou é de outro?{os.linesep}{os.linesep}/Mesmo{os.linesep}/Outro'''

        return AResposta
