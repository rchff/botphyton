import os


class Compra:
    def __init__(self):
        self.codigo = None

    def IniciarCFOP(self):
        return 'Para lhe responder isso, você consegue me informar qual a CFOP que você recebeu na sua nota?'

    def GetCFOP(self, mensagem):

        AResposta = ''
        if (mensagem == 'sim'):
            AResposta = 'Qual a CFOP que você recebeu na sua nota?'
            self.codigo = None

        elif (mensagem == '6404'):
            AResposta = f'''A cfop que você pode utilizar é: 2403{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''

        else:
            AResposta = 'A cfop que você pode utilizar é: '
            AOpcoes = {'5': '1', '6': '2', '7': '3'}

            if (mensagem[0:1] in AOpcoes.keys()):
                AResposta += AOpcoes.get(mensagem[0:1]) + mensagem[1:4]
                AResposta += f'''{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''
            else:
                AResposta = 'Opção inválida, verifique o CFOP e tente novamente'

        return AResposta
