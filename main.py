import json
import os

import nltk
import requests

from helpers import Helpers
from router import Router

stopwords = nltk.corpus.stopwords.words('portuguese')

saudacoes = ['oi',
             'ola',
             'bom dia',
             'boa tarde',
             'boa noite',
             ]

palavra_chave = ['com',
                 'ven',
                 'dev',
                 'bon'
                 ]


class TelegramBot:

    def __init__(self):
        token = '1969913123:AAGND_g0mlHj9asr5BVPJAlhLOrzYEVHCG4'
        self.url_base = f'https://api.telegram.org/bot{token}/'
        self.assunto = None
        self.obj = None

    def Iniciar(self):
        update_id = None
        while True:
            atualizacao = self.obter_novas_mensagens(update_id)
            dados = atualizacao["result"]
            if dados:
                for dado in dados:
                    update_id = dado['update_id']
                    mensagem = str(dado["message"]["text"])
                    chat_id = dado["message"]["from"]["id"]
                    eh_primeira_mensagem = int(
                        dado["message"]["message_id"]) == 1
                    resposta = self.criar_resposta(
                        mensagem, eh_primeira_mensagem)
                    self.responder(resposta, chat_id)

    # Obter mensagens
    def obter_novas_mensagens(self, update_id):
        link_requisicao = f'{self.url_base}getUpdates?timeout=100'
        if update_id:
            link_requisicao = f'{link_requisicao}&offset={update_id + 1}'
        resultado = requests.get(link_requisicao)
        return json.loads(resultado.content)

    # Criar uma resposta

    def criar_resposta(self, mensagem, eh_primeira_mensagem):

        mensagem_tratada = Helpers.remove_acentuacao(mensagem).replace("/", "")

        if ((eh_primeira_mensagem == True) or (mensagem_tratada in saudacoes)):
            return f'''Bem vindo, tudo bem com você?{os.linesep}Eu sou o Contabot, posso lhe auxiliar em suas principais dúvidas fiscais.{os.linesep}{os.linesep}Por gentileza selecione uma palavra-chave:{os.linesep}/CFOP{os.linesep}/CST{os.linesep}/CSOSN{os.linesep}'''

        elif (mensagem_tratada in ['sair', 'exit']):
            self.assunto = None
            self.obj = None
            return 'Obrigado por utilizar o ContaBot'

        elif (self.assunto == None):
            AOpcoesNavegacao = {
                'cfop': 'O seu objetivo é uma venda, compra, devolução, ou bonificação?',
                'cst': 'O seu objetivo é uma venda ou devolução',
                'csosn': 'O seu objetivo é uma venda ou devolução'
            }

            if (mensagem_tratada in AOpcoesNavegacao.keys()):
                self.assunto = mensagem_tratada
                return AOpcoesNavegacao.get(mensagem_tratada)
            else:
                return 'Opção inválida, tente novamente'

        elif (self.obj != None):

            if (self.assunto == 'cfop'):
                return self.obj.GetCFOP(mensagem_tratada)
            if (self.assunto == 'cst' or self.assunto == 'csosn'):
                return self.obj.GetCST(mensagem_tratada)
            else:
                return 'Erro Geral - Caminho não mapeado'

        elif (self.assunto != None):
            # transforma a mensagem em um array de palavras
            Alist = mensagem_tratada.split(' ')

            # analisa mensagem e pega a palavra chave da função
            for AItem in Alist:
                if (AItem[0:3] in palavra_chave):
                    self.obj = Router.Link(AItem[0:3])

            if self.obj != None:
                if (self.assunto == 'cfop'):
                    return self.obj.IniciarCFOP()
                else:
                    return self.obj.IniciarCST()
            else:
                return 'Opção inválida, tente novamente'

        else:
            return 'Opção inválida, tente novamente'

    def responder(self, resposta, chat_id):
        link_requisicao = f'{self.url_base}sendMessage?chat_id={chat_id}&text={resposta}'
        requests.get(link_requisicao)


if __name__ == '__main__':
    bot = TelegramBot()
    bot.Iniciar()
