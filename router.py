
from bonificacao import Bonificacao
from compra import Compra
from devolucao import Devolucao
from venda import Venda


class Router:

    def Link(AItem):
        AOpcoes = {
            'ven': Venda(),
            'com': Compra(),
            'dev': Devolucao(),
            'bon': Bonificacao()
        }

        if (AItem in AOpcoes.keys()):
            return AOpcoes.get(AItem)
        else:
            return 'Opção inválida, tente novamente'
