import os


class Devolucao:
    def __init__(self):
        self.codigo = None
        self.regime = None

    def IniciarCFOP(self):
        return f'''O produto que você está devolvendo, ele é tributado?{os.linesep}Para verificar essa informação, pegue a nota de compra desse produto e verifique a CST/CSOSN e me informe ele.'''

    def IniciarCST(self):
        return f'''Você é do regime Simples ou do Normal?{os.linesep}{os.linesep}/Simples{os.linesep}/Normal'''

    def GetCFOP(self, mensagem):

        AResposta = ''
        if (self.codigo == None and mensagem[0:3] in ['102', '000', '500', '060']):
            self.codigo = mensagem[0:3]
            AResposta = f'''O seu cliente mora no seu estado, ou é de outro?{os.linesep}{os.linesep}/Mesmo{os.linesep}/Outro'''

        elif (mensagem == 'sim'):
            AResposta = 'Informe o novo CST/CSOSN: '
            self.codigo = None

        elif (self.codigo in ['102', '000']):
            AResposta = 'A cfop que você pode utilizar é: '

            if (mensagem == 'mesmo'):
                AResposta += '5202'

            elif (mensagem == 'outro'):
                AResposta += '6202'

            AResposta += f'''{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''

        elif (self.codigo in ['500', '060']):
            AResposta = 'A cfop que você pode utilizar é: '

            if (mensagem == 'mesmo'):
                AResposta += '5411'

            elif (mensagem == 'outro'):
                AResposta += '6411'

            AResposta += f'''{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''

        else:
            AResposta = 'Opção inválida, verifique o CST/CSOSN e tente novamente'

        return AResposta

    def GetCST(self, mensagem):
        AResposta = ''
        if (self.regime == None and mensagem in ['simples', 'normal']):
            self.regime = mensagem
            AResposta = 'Na nota de compra desse produto, qual a CST/CSOSN utilizada por seu fornecedor?'

        elif (mensagem == 'sim'):
            AResposta = f'''Você é do regime Simples ou do Normal?{os.linesep}{os.linesep}/Simples{os.linesep}/Normal'''
            self.regime = None

        elif (self.regime == 'simples' and mensagem in ['102']):
            AResposta = 'A CSOSN que você pode utilizar é: 102'
            AResposta += f'''{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''
        elif (self.regime == 'simples' and mensagem in ['500']):
            AResposta = 'A CSOSN que você pode utilizar é: 500'
            AResposta += f'''{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''
        elif (self.regime == 'simples' and mensagem in ['000', '060']):
            AResposta = 'A CSOSN que você pode utilizar é: 900'
            AResposta += f'''{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''
        elif (self.regime == 'normal' and mensagem in ['102', '000']):
            AResposta = 'A CST que você pode utilizar é: 000'
            AResposta += f'''{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''
        elif (self.regime == 'normal' and mensagem in ['500', '060']):
            AResposta = 'A CST que você pode utilizar é: 060'
            AResposta += f'''{os.linesep}{os.linesep}Gostaria de realizar uma nova consulta?{os.linesep}/Sim{os.linesep}/Sair'''

        else:
            AResposta = 'Opção inválida, verifique e tente novamente'

        return AResposta
