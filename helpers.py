import unicodedata


class Helpers:

    def remove_acentuacao(string: str) -> str:
        normalize = unicodedata.normalize('NFD', string)
        return normalize.encode('ascii', 'ignore').decode('utf8').casefold()
